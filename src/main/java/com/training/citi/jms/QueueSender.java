package com.training.citi.jms;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class QueueSender implements ApplicationRunner {

    @Autowired
    JmsTemplate jmsTemplate;

    public void run(ApplicationArguments appArgs) {
        sendSimpleMessage("Hello World!");
    }

    public void sendSimpleMessage(String message) {  
        String messageToSend = message + " [" + new Date() + "]";
        System.out.println("Sending: " + messageToSend);
        jmsTemplate.convertAndSend("simpleDest", messageToSend);    
    }
}
