package com.training.citi.jms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJmsHelloworldApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringJmsHelloworldApplication.class, args);
	}

}
