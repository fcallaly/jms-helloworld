package com.training.citi.jms;

import java.util.Date;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class QueueListener {

    @JmsListener(destination="simpleDest")
    public void receiveSimpleMessage(String message) {
        String str = "Simple message received [" + new Date() + "] " + message;
        System.out.println(str);
    }
}

