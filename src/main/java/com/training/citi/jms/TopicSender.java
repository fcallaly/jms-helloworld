package com.training.citi.jms;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class TopicSender implements ApplicationRunner {

    @Autowired
    @Qualifier("jmsTopicTemplate")
    private JmsTemplate jmsTemplate;

    @Override
    public void run(ApplicationArguments appArgs) {
        String msg = "HelloTopic";

        System.out.println("Sending to Topic: " + msg);
        jmsTemplate.convertAndSend("tradeTopic", msg);
    }
}
